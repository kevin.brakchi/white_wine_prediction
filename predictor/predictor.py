import configparser               # To parse parameters in input file
import json                       # To parse and dump JSON
import sys
from kafka import KafkaProducer, KafkaConsumer
import numpy as np

# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_in = config['topic']['topic_in']
    topic_out = config['topic']['topic_out']
    return broker,topic_in,topic_out

# Parameters extracted from predictor.params
broker,topic_in,topic_out= get_params(sys.argv[1])

#Consume the training data
consumer = KafkaConsumer(topic_in,                   # Topic name
  bootstrap_servers = broker,                        # List of brokers passed from the command line
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)

#Producer to send the predictions
producer = KafkaProducer(
bootstrap_servers = broker,
value_serializer=lambda v: json.dumps(v).encode('utf-8'),
key_serializer=str.encode) 

# Generate the message sent in the topic sample.
def process_msg(msg):
  prediction = msg['prediction']
  label = msg['label']
  error = msg['Abs-error']
  tree = msg['tree']
  return prediction,label,error,tree

X = {}
y = {}
global_error = []
i=0


for msg in consumer:        # Blocking call waiting for a new message
    key = msg.key
    prediction,label,error,tree = process_msg(msg.value)
    if key in X :
      X[key].append(prediction)
    else :
      X.update({key: [prediction]})
      y.update({key: label})
    for key in X :
      if len(X[key]) > 3 :
        fkey = key
        train = X[key]
        label = y[key]
        # Compute the average prediction from the 4 trees
        final_prediction = sum(train)/len(train)
        # Compute the absolute error from the average prediction
        final_error = abs(final_prediction - label)
        global_error.append(final_error)
        #Exempty the predictions from the dictionnary
        X[key] = []
        fmsg = {
        'final_prediction': round(final_prediction,2),
        'label': label,
        'final_error': round(float(final_error),2),
        'global_error': round(np.mean(global_error),2)
        }
        # Send the message to the models topic
        producer.send(topic_out, key = str(key), value = fmsg, partition=0)
        producer.send(topic_out, key = str(key), value = fmsg, partition=1)
        #Print results for log aggregation
        print(topic_out,"key :",key,"value :",fmsg)
        
# Flush: force purging intermediate buffers before leaving
producer.flush()