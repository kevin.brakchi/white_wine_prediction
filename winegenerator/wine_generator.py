import configparser               # To parse parameters in input file
import json                       # To parse and dump JSON
from kafka import KafkaProducer   # Import Kafka producer
import sys

# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_out = config['topic']['topic_out']
    dataset = str(config['parameters']['dataset'])
    return broker,topic_out,dataset

# Parameters extracted from params file
broker,topic_out,dataset= get_params(sys.argv[1])

producer = KafkaProducer(
  bootstrap_servers = broker,                     # List of brokers passed from the command line
  value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
  key_serializer=str.encode                                 # How to serialize the key
)

#Extract data from the csv file.
while True :
  with open(dataset) as file:
      content = file.readlines()
  header = content[:1]
  rows = content[1:]
  j =0
  for i in rows :
    
    producer.send(topic_out, key = str(j), value = i,partition = 0) # Send a new message to topic in partition 0
    producer.send(topic_out, key = str(j), value = i,partition = 1) # Send a new message to topic in partition 1
    producer.send(topic_out, key = str(j), value = i,partition = 2) # Send a new message to topic in partition 2
    producer.send(topic_out, key = str(j), value = i,partition = 3) # Send a new message to topic in partition 3

    #Sends data to the white-wine topic.
    print(topic_out, "key :",str(j),"valeur :", i)
    j+=1
    producer.flush()
