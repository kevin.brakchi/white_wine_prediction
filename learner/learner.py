import configparser               # To parse parameters in input file
import json                       # To parse and dump JSON
import sys
from sklearn.ensemble import RandomForestRegressor
from kafka import KafkaProducer, KafkaConsumer
import numpy as np
from kafka.structs import TopicPartition

# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_train = config['topic']['topic_train']
    topic_test = config['topic']['topic_test']
    topic_out = config['topic']['topic_out']
    tree_id = int(config['parameters']['tree_id'])
    partition_in = int(config['parameters']['partition_in'])
    return broker,topic_train,topic_test,topic_out,tree_id,partition_in

# Parameters extracted from learner.params
broker,topic_train,topic_test,topic_out,tree_id,partition_in= get_params(sys.argv[1])

#Consume the training data
consumer = KafkaConsumer(
  bootstrap_servers = broker,                        # List of brokers passed from the command line
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)

#Compute only messages in the partition 3
consumer.assign([TopicPartition(topic_train, partition_in)])

#Consume the test data
test_consumer = KafkaConsumer(
  bootstrap_servers = broker,                        # List of brokers passed from the command line
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)

#Collect only messages in the partition 3
test_consumer.assign([TopicPartition(topic_test, partition_in)])

#Producer to send the predictions
producer = KafkaProducer(
bootstrap_servers = broker,
value_serializer=lambda v: json.dumps(v).encode('utf-8'),
key_serializer=str.encode) 
  
X = []
y = []
i=0
for msg in consumer:        # Blocking call waiting for a new message
    akey = msg.key
    #"fixed acidity";"volatile acidity";"citric acid";"residual sugar";"chlorides";"free sulfur dioxide";"total sulfur dioxide";"density";"pH";"sulphates";"alcohol";"quality"
    fac,volac,citac,sugar,chlor,fsuldio,suldio,dens,pH,sulph,alco,qual = msg.value.split(";")
    temp = [fac,volac,citac,sugar,chlor,fsuldio,suldio,dens,pH,sulph,alco]
    X.append(temp)
    y.append(qual)
    i+=1
    #At least 4000 sample is required for the regression to be effective.
    if i > 3900 :
      model =  RandomForestRegressor(bootstrap=True, criterion='squared_error', max_depth=None,
           max_features='sqrt', max_leaf_nodes=None,
           min_impurity_decrease=0.0,
           min_samples_leaf=1, min_samples_split=2,
           min_weight_fraction_leaf=0.0, n_estimators=400, n_jobs=1,
           oob_score=False, random_state=None, verbose=0, warm_start=False) ## random forest model
      model.fit(X,y)
      for tmsg in test_consumer :
        tkey = tmsg.key
        test = []
        test_labels = []
        temp = tmsg.value.split(";")
        test_labels.append(float(temp[-1]))
        temp.pop()
        test.append(temp)
        # Use the forest's predict method on the test data
        predictions = model.predict(test)
        # Calculate the absolute errors
        errors = abs(np.array(predictions) - np.array(test_labels))
        pmsg = {
        'prediction': predictions[0],
        'label': test_labels[0],
        'Abs-error': float(round(np.mean(errors), 2)),
        'tree' : tree_id
        }
        
        producer.send(topic_out, key = str(tkey), value = pmsg, partition = 0)
        producer.send(topic_out, key = str(tkey), value = pmsg, partition = 1)

        # Send the message to the models topic
        print(topic_out, "key :",str(tkey),"valeur :", pmsg)
# Flush: force purging intermediate buffers before leaving
producer.flush()
