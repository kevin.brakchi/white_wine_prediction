White wine quality prediction
---------------
A data science project realised in an academic environment to predict the quality of white wines.

![alt text](https://253qv1sx4ey389p9wtpp9sj0-wpengine.netdna-ssl.com/wp-content/uploads/2019/09/White_Wine_Getty_HERO_1920x1280.jpg)

![alt text](https://www.centralesupelec.fr/sites/all/themes/cs_theme/medias/common/images/intro/logo.png)

Author :
---------------
- [Kevin Brakchi](mailto:kevin.brakchi@student-cs.fr>)

Description :
---------------
This project's goal is to predict the quality of a white wine knowing its following characteristics:
- fixed acidity
- volatile acidity
- citric acid
- residual sugar
- chlorides
- free sulfur dioxide
- total sulfur dioxide
- density
- pH
- sulphates
- alcohol

To perform this task, a dataset composed of 4898 samples containing the characteristics and assigned quality of different wine is at our disposal. 

To verify the performances of our model, this dataset is split in two in order to create a train_dataset containing 80% of samples and a test_dataset containing the remaining 20% of samples.

In this project, the predictor is composed multiple random forest regressors, fine-tuned with a grid-search method and averaged to assign to each wine of the test_dataset the most accurate quality prediction possible.

Project Organization :
------------

    ├── LICENSE
    │
    ├── README.md          <- The top-level README for developers using this project.
    │
    │
    ├── docker_compose     <- Folder containing the docker-compose yaml files tor the local deployment of the application's containers.
    │   │
    │   ├── docker-compose-middleware.yml       
    │   │
    │   └── docker-compose-services.yml
    │
    │
    ├── kubernetes  <- Folder containing the yaml files tor the deployment of the application on a cluster.
    │   │
    │   ├── wine-estimator.yml   
    │   │
    │   └── zookeeper-and-kafka_v3.yml
    │
    │
    ├── learner  <- Folder containing the python scripts to train the different random forests regressors, parameter files and docker files to deploy them. 
    │   │
    │   ├── tree1
    │   │    │
    │   │    ├── Dockerfile.tree1 
    │   │    │
    │   │    └── tree1.params
    │   │    
    │   ├── tree2
    │   │    │
    │   │    ├── Dockerfile.tree2 
    │   │    │
    │   │    └── tree2.params
    │   │    
    │   ├── tree3
    │   │    │
    │   │    ├── Dockerfile.tree3
    │   │    │
    │   │    └── tree3.params
    │   │
    │   ├── tree4
    │   │    │
    │   │    ├── Dockerfile.tree4
    │   │    │
    │   │    └── tree4.params
    │   │    
    │   └── learner.py         
    │   
    │
    ├── predictor  <- Folder containing the python scripts and paramaters for the final prediction, parameters + docker files for the deployment.
    │   │
    │   ├── Dockerfile.predictor
    │   │   
    │   ├── predictor.params
    │   │
    │   └── predictor.py
    │
    │
    ├── resources  <- Folder containing resources for the project's presentation.
    │   │
    │   ├── Grid_search_optimizer.ipynb <- Noteboock used for the implementation of the grid search method on the random forest regressors.
    │   │
    │   └── Projet_White_Wine_prediction_architecture.png                             
    |
    |
    └── winegenerator <- Folder containing the scripts for the data extraction.
        │
        ├── datasets       <- Folder containing the datasets used by the application.
        │   │
        │   ├── test_dataset.csv
        │   │
        │   └── train_dataset.csv
        │
        ├── test_data       <- Parameters and docker files to deploy a kafa data stream with the testing data.
        │   │
        │   ├── Dockerfile.test
        │   │
        │   └── test_data.params
        │
        ├── train_data         <- Parameters and docker files to deploy a kafa data stream with the training data.
        │   │
        │   ├── Dockerfile.train
        │   │
        │   └── train_data.params
        │
        └── wine_generator.py  <- Python scipt to generate a kafa data stream.

Tech :
---------------
The created solution is based on the following technologies:

- [Python] - A programming language adapted to the creation of Kafka data pipelines.
- [Kafka] - An open-source distributed event streaming platform.
- [Docker] - An open platform for developing, shipping and running applications, used for a simple and reliable deployment of our application.
- [Gitlab CI/CD] - A tool for software development using the continous methodologies, useful to keep an historic of the project's development and for continuous integration and deployment purposes.
- [Kubernetes] - An open-source container orchestration platform used to make our solution scalable and more resilient.

Application's architecture :
---------------
![alt text](resources/Projet_White_Wine_prediction_architecture.png?raw=true "Project's architecture")

The goal of this project is to implement a scalable and maintainable application. The architecture has been developed with these considerations in mind. 

**Generator :**

Train_data_generator: Collect data from the training dataset csv file and sends them into the "white_wine" Kafka topic.

Test_data_generator: Collect data from the testing dataset csv file and sends them into the "test_white_wine" Kafka topic.

**Learner :**

Trees: Collect data from the "white_wine" and "test_white_wine" topics to train 4 random forest regressors with different parameters in order to predict the quality of a white wine. Once the different trees, represented by tree1, tree2, tree3 and tree4 are trained, they send their predictions and individual absolute error to the "tree_predictions" topic.

**Predictor :**

Predictor: Collect the data from the "tree_predictions" topic to create a final prediction for the different tested wine by averaging the predictions of the 4 different trees. This process aims at improving the performance of the application. It also sends statistics on the performance of the predictions such as the individual absolute error and the global absolute error averaged on all the predictions.These data are finally sent on the "final_results" to make them accessible.


## Docker-compose deployment :
You need to install docker and docker-compose on your device to implement this deployment.

```sh
sudo apt install docker-compose
cd docker_compose
#Deploy the kafka and zookeeper containers
docker-compose -f docker-compose-middleware.yml up

#Deploy the application's containers
docker-compose -f docker-compose-services.yml up
```

## Minikube deployment :
You need to install minikube on your device to implement this deployment.

```sh
cd kubernetes
#Launch the minkube local cluster
minikube start

#Start the kafka and zookeeper pods
kubectl apply -f zookeeper-and-kafka_v3.yml
kubectl get pods
#Wait 30 seconds for the creation of the different kafka topics

#Deploy the different pods and services of the application
kubectl apply -f wine-estimator.yml
```

To check the logs of the deployed application : 
```sh
kubectl logs predictor-pod
```
## Intercell deployment :
```sh
cd kubernetes

# cpu connection
ssh cpusdi1_XX@phome.metz.supelec.fr
<password>
ssh <assigned host>

# copy files to cluster
scp zookeeper-and-kafka_v3.yml cpusdi1_XX@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_XX
scp wine-estimator.yml cpusdi1_XX@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_XX

# run project
kubectl -n cpusdi1-XX-ns apply -f zookeeper-and-kafka_v3.yml
kubectl -n cpusdi1-XX-ns get pods

# wait for kafka
kubectl -n cpusdi1-XX-NS apply -f wine-estimator.yml.yml
```

## Fault tolerance :
The deployment is possible on multiple nodes of a kubernetes cluster, by assigning at least three nodes we remove the single points of failures, thus decreasing significantly the risk of failures.
Furthermore, every part of the application is on an independent pod that can be monitored to detect and locate emerging problems.
Also we can add replication factors of at least 2 to remove single point of failures in the kafka data stream.

## Scalability :
The application can be possible on multiple nodes of a kubernetes cluster, making it innerly scalable.
In addition, there is multiple partitions per topic to treat simultaneously a large quantity data and ensure the data scalability of the application.
Random forest trees are also working in parallel to accelerate the training, it is therefore possible to increase the number of trees and partitions if the size of the input dataset increases.